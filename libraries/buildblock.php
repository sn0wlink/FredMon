<?php

// This function builds the main page blocks.

// Buildblock (Header Name, Text to display)
function buildblock($header, $blockinput) {
        echo "<div class='display-block'>";
        echo "<div class='block-header'>$header</div>";
        echo "<br />";
        echo $blockinput;
        echo "</div>";
}

?>