<?php

if ($display_ip == true) {

    // Get System IP Address
    $getip = shell_exec('hostname -I');
    $systemip = str_ireplace(' ','',$getip);


    // if systemip contains "."
    if (strpos($systemip, '.') !== false) {
        buildblock('IP Address', $systemip);
    }

    // else display no IP
    else {
        buildblock('IP Address', 'No Ip Found');
    }

}

?>