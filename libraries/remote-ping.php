<?php

    /*
        -= REMOTE PING =-

        This library constructs the remote
        server ping, and notifies the user
        of servers that are unavailable.
    */

function RemoteServer () {

    // Import Remote Server Settings
    foreach (glob('servers/*') as $file) {
        include ($file);

        // Declare variabubbles
        $samba = "";
        $http = "";
        $https = "";
        $smtp = "";
        $ftp = "";
        $ssh = "";


        // Ping The Different Ports

        if ($PingSamba == true) {
            $samba = SrvPing('Samba', $SrvIP, 445);
        }

        if ($PingHTTP == true) {
            $http = SrvPing('HTTP', $SrvIP, 80);
        }

        if ($PingHTTPS == true) {
            $https = SrvPing('HTTPS', $SrvIP, 443);
        }

        if ($PingSMTP == true) {
            $smtp = SrvPing('SMTP', $SrvIP, 25);
        }

        if ($PingFTP == true) {
            $ftp = SrvPing('FTP', $SrvIP, 21);
        }

        if ($PingSSH == true) {
            $ssh = SrvPing('SSH', $SrvIP, 22);
        }

        else {

        }

        $input = "
            Server IP: $SrvIP <br /><br />

            $http
            $https
            $smtp
            $ftp
            $ssh
            $samba
        ";


        buildblock($SrvName, $input);

    }

}


function SrvPing($ServiceName,$IpAddress,$Port) {
    if (!$socket = @fsockopen($IpAddress, $Port, $errno, $errstr, 0.1)) {
        $PingOutput = "
            <div style='color: red'>
                <strong>$ServiceName - OFFLINE</strong>
            </div>";
    }

    else
    {
        $PingOutput = "$ServiceName - Online <br />";
        
    }
    fclose($socket);
    return $PingOutput;
}


?>