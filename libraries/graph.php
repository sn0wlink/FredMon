<?php

/*

////////////////////////////////////////////////////////////////////////////////
//                              HOW TO USE
////////////////////////////////////////////////////////////////////////////////

Example of a single bar

SingleBar(
    25,         // Variable
    100,        // Variable out of
    200,        // Bar width
    10,         // Bar height
    '#5daffc',  // Bar colour
    '#cccccc'   // Background colour
);

Example for a Bargraph

BarGraph(
    25,         // Variable
    100,        // Variable out of
    200,        // Bar width
    10,         // Bar height
    '#5daffc',  // Bar colour
    '#cccccc'   // Background colour
);

Used in a block
echo "<div class='bargraph1'>";
    BarGraph(20,100,10,200,'#5daffc','#cccccc');
    BarGraph(80,100,10,200,'#5daffc','#cccccc');
    BarGraph(60,100,10,200,'#5daffc','#cccccc');
echo "</div>";

*/


////////////////////////////////////////////////////////////////////////////////
//                              SINGLE BAR
////////////////////////////////////////////////////////////////////////////////

// Bar Graph Generator
Function SingleBar($value,$total,$barwidth,$barheight,$barcolour,$bgcolour) {

    // Calculates bargraph dimensions
    $ratio = $barwidth/$total;
    $barvaluewidth = $ratio*$value;

    // Print to screen
    echo "
        <div style='
            background-color: $bgcolour;
            height: $barheight;
            width: $barwidth;'>
                <div style='
                    background-color: $barcolour;
                    height: $barheight;
                    width: $barvaluewidth;'>
                </div>
        </div>
    ";
}

////////////////////////////////////////////////////////////////////////////////
//                               Bar Graph
////////////////////////////////////////////////////////////////////////////////

Function BarGraph(
    $value,
    $total,
    $barwidth,
    $barheight,
    $barcolour,
    $bgcolour
    ) {

    // Calculates bargraph dimensions
    $ratio = $barheight/$total;
    $barvalueheight = $ratio*$value;

    // Print to screen
    echo "
        <div class='Bar' style='
            position: relative;
            display: inline-block;
            background-color: $bgcolour;
            height: $barheight;
            width: $barwidth;'>
                <div style='
                    position: absolute;
                    bottom: 0;
                    left: 0;
                    background-color: $barcolour;
                    width: $barwidth;
                    height: $barvalueheight;'>
                </div>
        </div>
    ";
}


?>
