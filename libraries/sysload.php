<?php 

// include ('./config/config.php');

$cpu = CPULoad();
$ram = RAMLoad();

if ($display_sysload == true) {
    buildblock('System Load', "CPU: $cpu%<br />Ram Usage: $ram%");
}

// Get CPU Load
function CPULoad(){
    $load = sys_getloadavg();
    return $load[0];
}

// Get RAM Usage Percentage
function RAMLoad(){
    $free = shell_exec('free');
    $free = (string)trim($free);
    $free_arr = explode("\n", $free);
    $mem = explode(" ", $free_arr[1]);
    $mem = array_filter($mem);
    $mem = array_merge($mem);
    $memory_usage = $mem[2]/$mem[1]*100;
    return number_format($memory_usage, 2);
}

?>