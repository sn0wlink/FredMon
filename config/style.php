/*

    FredMon
    David Collins-Cubit
	2019

*/

.display-block {
    font-family: helvetica, arial, sans;
    font-size: 10pt;
    background-color: #accaf9;
    width: 200px;
    height: 200px;
    padding:10px;
    margin:10;
    float: left;
}

.block-header {
    font-family: helvetica, arial, sans;
    font-weight: bold;
    font-size: 12pt;
}