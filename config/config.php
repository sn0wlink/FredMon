<?php

// Website Name
$controlname = 'FredMon';

// Display Libraries as a list (for testing)
$display_libraries = false;

// Display Stats
$display_ip = false;
$display_uptime = false;
$display_os = false;
$display_sysload = false;
$disk_use = false; // Currently Testing

// Refresh Page
$page_refresh = true;
$refresh_seconds = "3"; // Refresh every (seconds)

?>